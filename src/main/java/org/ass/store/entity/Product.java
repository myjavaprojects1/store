package org.ass.store.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="product_info")
public class Product {
	@Id
	@GenericGenerator(name="reg_auto",strategy="increment")
	@GeneratedValue(generator="reg_auto")
	@Column(name="alt_key")
	private int altKey;
	
	@Column(name="product_name")
	private String name;
	
	@Column(name="product_price")
	private long price;
	
	@Column(name="created_date")
	private LocalDate CreatedDate;
	
	@Column(name="modified_date")
	private LocalDate modifiedDate;

	public int getAltKey() {
		return altKey;
	}

	public void setAltKey(int altKey) {
		this.altKey = altKey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public LocalDate getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		CreatedDate = createdDate;
	}

	public LocalDate getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(LocalDate modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	public String toString() {
		return "Product [altKey=" + altKey + ", name=" + name + ", price=" + price + ", CreatedDate=" + CreatedDate
				+ ", modifiedDate=" + modifiedDate + "]";
	}
	
	
}
