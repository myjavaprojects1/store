package org.ass.store.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="stock_info")
public class Stock {
	
	@GenericGenerator(name="reg_auto",strategy="increment")
	@GeneratedValue(generator="reg_auto")
	@Column(name="alt_key")
	private int altKey;
	
	@Id
	@Column(name="product_id")
	private int productId;
	
	@Column(name="stock_number")
	private int stockNumber;

	public int getAltKey() {
		return altKey;
	}

	public void setAltKey(int altKey) {
		this.altKey = altKey;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getStockNumber() {
		return stockNumber;
	}

	public void setStockNumber(int stockNumber) {
		this.stockNumber = stockNumber;
	}
	
	
		

}
