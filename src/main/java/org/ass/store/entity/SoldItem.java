package org.ass.store.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="sold_item_info")
public class SoldItem {
	@Id
	@GenericGenerator(name="reg_auto",strategy="increment")
	@GeneratedValue(generator="reg_auto")
	@Column(name="alt_key")
	private int altkey;
	
	@Column(name="product_id")
	private int productId;
	
	@Column(name="product_quantity")
	private int quantity;
	
	@Column(name="total_price")
	private long totalPrice;
	
	@Column(name="purchase_id")
	private String purchaseId;
	
	@Column(name="created_date")
	private LocalDate createdDate;

	public int getAltkey() {
		return altkey;
	}

	public void setAltkey(int altkey) {
		this.altkey = altkey;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public long getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}

	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "SoldItem [altkey=" + altkey + ", productId=" + productId + ", quantity=" + quantity + ", totalPrice="
				+ totalPrice + ", purchaseId=" + purchaseId + ", createdDate=" + createdDate + "]";
	}
	

}
