package org.ass.store.util;
import org.ass.store.entity.Product;
import org.ass.store.entity.SoldItem;
import org.ass.store.entity.Stock;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryUtil {
	private static SessionFactory sessionFactory;
	public SessionFactoryUtil() {}
	
	static{
		Configuration cfg=new Configuration();
		cfg.setProperties(ConnectionPropertiesUtil.getConnectionProperties());
		cfg.addAnnotatedClass(Product.class);
		cfg.addAnnotatedClass(Stock.class);
		cfg.addAnnotatedClass(SoldItem.class);
		sessionFactory=cfg.buildSessionFactory();
	}
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}	
}
