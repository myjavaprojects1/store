package org.ass.store.dao;

import java.util.List;

import org.ass.store.dto.OrderDto;
import org.ass.store.dto.ProductDto;

public interface StoreDao {
	public void addProduct(ProductDto productDto);
	public void sellItem(List<OrderDto> dtoList);
	public Object getSoldProductReportForCurrentDate(int productId);

}
