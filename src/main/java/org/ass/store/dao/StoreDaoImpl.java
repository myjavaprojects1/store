package org.ass.store.dao;

import java.util.List;

import org.ass.store.dto.OrderDto;
import org.ass.store.dto.ProductDto;
import org.ass.store.entity.Product;
import org.ass.store.entity.SoldItem;
import org.ass.store.entity.Stock;
import org.ass.store.util.DateUtil;
import org.ass.store.util.SessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class StoreDaoImpl implements StoreDao {

	@Override
	public void addProduct(ProductDto productDto) {
		Product p = new Product(); 
		Stock stock = new Stock();
		//p.setAltKey(1000);
		p.setName(productDto.getName());
		p.setPrice(productDto.getPrice());
		p.setCreatedDate(productDto.getCreatedDate());
		p.setModifiedDate(productDto.getModifiedDate());
		
	    
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Product product = (Product)session.merge(p);
		
		
		stock.setProductId(product.getAltKey());
		stock.setStockNumber(productDto.getQuantity());
		
		session.merge(stock);
		transaction.commit();
	}

	@Override
	public void sellItem(List<OrderDto> dtoList) {
		SoldItem soldItem = new SoldItem(); 
		for(OrderDto orderDto:dtoList) {
		soldItem.setProductId(orderDto.getProductId());
		soldItem.setQuantity(orderDto.getQuantity());
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Product product = session.get(Product.class,orderDto.getProductId());
		Stock stock = session.get(Stock.class,orderDto.getProductId());
		product.setModifiedDate(orderDto.getCreatedDate());
		stock.setStockNumber(stock.getStockNumber()-orderDto.getQuantity());
		soldItem.setTotalPrice(product.getPrice()*orderDto.getQuantity());
		soldItem.setPurchaseId("qert-7654-cbnj");
		soldItem.setCreatedDate(orderDto.getCreatedDate());
		
		
		Transaction transaction = session.beginTransaction();
		session.merge(soldItem);
		session.merge(product);
		session.merge(stock);
		
		transaction.commit();
		}
	}

	@Override
	public Object getSoldProductReportForCurrentDate(int productId) {
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		StringBuilder builder = new StringBuilder();
		builder.append("from SoldItem where createdDate=:d and productId=:p");
		Query query = session.createQuery(builder.toString());
		query.setParameter("d", DateUtil.getCurrentDate());
		query.setParameter("p", productId);
		return query.getSingleResult();
	}

}
